﻿using System;

namespace Lab9
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student_1 = new Student("Solomonov", "Oleg", "Olegovich", 2);
            student_1.Live();
            Console.WriteLine(student_1.ToString());
            Person person_1 = student_1;
            person_1.Live();
            Console.WriteLine(person_1.ToString());

            Console.WriteLine();

            Tutor tutor_2 = new Tutor("Vanininin", "Artemid", "Valerievich", 20);
            tutor_2.Live();
            Console.WriteLine(tutor_2.ToString());
            Person person_2 = tutor_2;
            person_2.Live();
            Console.WriteLine(person_2.ToString());
        }
    }
}

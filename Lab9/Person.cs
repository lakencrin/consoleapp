﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab9
{
    class Person
    {
        public string Surname { get; protected set; }
        public string Name { get; protected set; }
        public string SecondName { get; protected set; }
        public string Nationality { get; protected set; }
        public string Address { get; set; }

        public virtual void Live()
        {
            Console.WriteLine("Person lives");
        }
        public override string ToString()
        {
            return Surname + " " + Name + " " + SecondName;
        }

        public Person(string _surname, string _name, string _second)
        {
            Surname = _surname;
            Name = _name;
            SecondName = _second;
        }
    }
}

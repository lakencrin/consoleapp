﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab9
{
    class Tutor : Person
    {
        List<string> m_disciplines;

        public string AcademicDegree { get; set; }
        public string Rank { get; set; }
        public int Experience { get; private set; } = 0;

        public void AddDiscipline(string discipline)
        {
            if (!m_disciplines.Contains(discipline))
                m_disciplines.Add(discipline);
        }
        public void RemoveDiscipline(string discipline)
        {
            m_disciplines.Remove(discipline);
        }
        public void AddExperience()
        {
            ++Experience;
        }

        public override void Live()
        {
            Console.WriteLine("Tutor lives");
        }
        public override string ToString()
        {
            return string.Format("Tutor {0}: {1}", AcademicDegree, base.ToString());
        }

        public Tutor(string _surname, string _name, string _second, int _exp, string _degree = "")
            : base(_surname, _name, _second)
        {
            Experience = _exp;
            AcademicDegree = _degree;
        }
    }
}

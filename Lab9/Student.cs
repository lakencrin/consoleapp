﻿using System;
using System.Collections.Generic;

namespace Lab9
{
    class Student : Person
    {
        Dictionary<string, int> _marks;

        public string Group { get; set; }
        public int AcademicYear { get; private set; }

        public void SetMark(string discipline, int mark)
        {
            _marks[discipline] = mark;
        }
        public int GetMark(string discipline)
        {
            if (!_marks.ContainsKey(discipline))
                throw new ArgumentException("No discipline in mark dictionary");

            return _marks[discipline];
        }
        public void MoveToNextYear() { ++AcademicYear; }

        public override void Live()
        {
            Console.WriteLine("Student lives");
        }
        public override string ToString()
        {
            return String.Format("Student, {0} year, group {1}: {2}", AcademicYear, Group, base.ToString());
        }

        public Student(string _surname, string _name, string _second, int _year) 
            : base(_surname, _name, _second)
        {
            AcademicYear = _year;
        }
    }
}

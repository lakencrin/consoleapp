﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab8_1
{
    static class Program
    {
        public static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.Add(new Note("Solomeev", "Oleg", new DateTime(2000, 5, 1), "+79879879879"));
            notebook.Add(new Note("Shabanovich", "Leonid", new DateTime(2001, 6, 11), "+79001234568"));
            notebook.Add(new Note("Pershkovskiy", "Alex", new DateTime(2002, 7, 12), "+79170000917"));
            notebook.Add(new Note("Brevnev", "Andrey", new DateTime(2003, 8, 13), "+79200299207"));
            Console.WriteLine(notebook);

            notebook.Sort();
            Console.WriteLine(notebook);

            notebook.Remove("+79001234568");
            Console.WriteLine(notebook);

            Note note = notebook.Find("+79200299207");
            Console.WriteLine(note);
        }
    }
}

﻿                                                using System;
using System.Collections.Generic;
using System.Text;

namespace Lab8_1
{
    class Notebook
    {
        List<Note> m_notes;

        public Notebook() 
        {
            m_notes = new List<Note>();
        }

        public void Add(Note note)
        {
            m_notes.Add(note);
        }
        public void Remove(string number)
        {
            m_notes.RemoveAll(_ => _.Number == number);
        }
        public Note Find(string number)
        {
            return m_notes.Find(_ => _.Number == number);
        }
        public int Count()
        {
            return m_notes.Count;
        }
        public Note Get(int index)
        {
            return m_notes[index];
        }
        public void Sort()
        {
            m_notes.Sort((_1, _2) => { return string.Compare(_1.Surname, _2.Surname, true); });
        }
        public override string ToString()
        {
            string result = "";
            m_notes.ForEach(_ => result += _.ToString() + '\n');
            return result;
        }
    }
}

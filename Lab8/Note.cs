﻿using System;

namespace Lab8_1
{
    class Note
    {
        public string Surname { get; private set; }
        public string Name { get; private set; }
        public DateTime Birthday { get; private set; }
        public string Number { get; private set; }

        public Note(string surname, string name, DateTime bday, string number)
        {
            Surname = surname;
            Name = name;
            Birthday = bday;
            Number = number;
        }
        
        public override string ToString()
        {
            return Surname + " " + Name + "\t\t\t" + Number + "\t" + Birthday;        }
    }
}

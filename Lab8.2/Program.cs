﻿using System;
using System.Collections.Generic;

namespace Lab8_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Airport airport = new Airport();
            airport.Add(new Plane("Moscow",             "AR1234", new DateTime(2020, 5, 20, 12, 0, 0)));
            airport.Add(new Plane("Saint Petersburg",   "AC7780", new DateTime(2020, 5, 20, 14, 0, 0)));
            airport.Add(new Plane("Moscow",             "FP0923", new DateTime(2020, 5, 20, 14, 30, 0)));
            airport.Add(new Plane("Krasnodar",          "PR2891", new DateTime(2020, 5, 20, 14, 55, 0)));
            airport.Add(new Plane("Nizhniy Novgorod",   "HU9080", new DateTime(2020, 5, 21, 10, 0, 0)));
            airport.Add(new Plane("Moscow",             "OI5437", new DateTime(2020, 5, 22, 5, 0, 0)));

            Console.WriteLine(airport.Count());
            Console.WriteLine(airport.GetPlane(0));
            Console.WriteLine(airport.GetPlane(4));
            Console.WriteLine(airport.GetPlane("OI5437"));
            Console.WriteLine(airport.GetPlane("FP0923"));
            Console.WriteLine();

            PrintPlanes(airport.GetRecentPlanes(new DateTime(2020, 5, 20, 14, 5, 0)));
            Console.WriteLine();

            PrintPlanes(airport.GetPlanesByDestination("Moscow"));
        }

        public static void PrintPlanes(List<Plane> planes)
        {
            planes.ForEach(_ => Console.WriteLine(_));
        }
    }
}

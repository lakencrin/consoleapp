﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab8_2
{
    class Plane : IComparable
    {
        public string Destination { get; private set; }
        public string FlightCode { get; private set; }
        public DateTime DepartureTime { get; private set; }

        public Plane(string dest, string code, DateTime dateTime)
        {
            Destination = dest;
            FlightCode = code;
            DepartureTime = dateTime;
        }
        public override string ToString()
        {
            return String.Format("Flight {0} to {1} at {2}", FlightCode, Destination, DepartureTime);
        }

        public static bool operator>(Plane left, Plane right)
        {
            return left.DepartureTime > right.DepartureTime;
        }
        public static bool operator<(Plane left, Plane right)
        {
            return left.DepartureTime < right.DepartureTime;
        }
        public int CompareTo(object? other)
        {
            Plane plane = other as Plane;
            if (plane is null)
                throw new ArgumentException();

            return this < plane ? -1 : this > plane ? 1 : 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab8_2
{
    class Airport
    {
        List<Plane> planes = new List<Plane>();

        public void Add(Plane plane)
        {
            planes.Add(plane);
            planes.Sort();
        }
        public int Count()
        {
            return planes.Count;
        }
        public Plane GetPlane(int index)
        {
            return planes[index];
        }
        public Plane GetPlane(string code)
        {
            return planes.Find(_ => _.FlightCode == code);
        }
        public List<Plane> GetRecentPlanes(DateTime dateTime)
        {
            return planes
                .Where(_ => { return (_.DepartureTime - dateTime) < TimeSpan.FromHours(1) && (_.DepartureTime.CompareTo(dateTime) > 0); })
                .ToList();
        }
        public List<Plane> GetPlanesByDestination(string destination)
        {
            return planes.Where(_ => _.Destination == destination).ToList();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10
{
    public partial class MainForm : Form
    {
        readonly InputDialog _inputDialog;
        readonly CalcDialog _calcDialog;
        readonly DrawDialog _drawDialog;

        int _radius = 0;
        bool _isLength = false;
        bool _isSquare = false;

        public MainForm()
        {
            InitializeComponent();
            _inputDialog = new InputDialog();
            _calcDialog = new CalcDialog();
            _drawDialog = new DrawDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == _inputDialog.ShowDialog())
            {
                _radius = _inputDialog.Radius;
                _isLength = _inputDialog.IsLength;
                _isSquare = _inputDialog.IsSquare;
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (_radius <= 0)
                return;

            _calcDialog.SetInfo(_radius, _isLength, _isSquare);
            _calcDialog.ShowDialog();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (_drawDialog.Draw(_radius))
                _drawDialog.ShowDialog();
            else 
                MessageBox.Show("Drawing is impossible", "Invalid input");
        }
        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10
{
    public partial class DrawDialog : Form
    {
        int _radius;
        public DrawDialog()
        {
            InitializeComponent();
            this.Paint += DrawDialog_Paint;
        }

        private void DrawDialog_Paint(object sender, PaintEventArgs e)
        {
            if (_radius < Width / 2 && _radius > 0)
            {
                Graphics myGraphics = e.Graphics;
                Pen pen = new Pen(Color.Red);
                myGraphics.DrawEllipse(pen, Width / 2 - _radius / 2, Height / 2 - _radius / 2, _radius, _radius);
            }
        }
    }
}
